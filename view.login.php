<!DOCTYPE html>
<html>
<head>
	<title>KU Admission Test-2017</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Raleway:300,400,600">

	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script> 
		$(function(){
			$("#includedContent").load("header.php");
			$("#footer").load("footer.php"); 
		});
	</script>
</head>

<div id="includedContent">
</div>
<body>

	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">

			<div class="panel panel-primary">
				<div class="panel-heading">
					Login To Upload Image and Download Admit Card
				</div>
				<div class="panel-body">
					<form action="login.php" method="post" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label" >Username</label>
							<div class="col-sm-6">
								<input type="text" name="username" value="" class="form-control"placeholder="Username">

							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" >Password</label>
							<div class="col-sm-6">
								<input type="password" name="password" value="" class="form-control">

							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-6">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-btn fa-plus"></i>Submit
								</button>
							</div>
						</div>
					</form>
					

				</div>
			</div>
			
		</div>
		
	</div>


	<div id="footer">
		
	</div>

</body>
</html>