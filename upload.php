
<!DOCTYPE html>
<html>
<head>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
 <meta charset="UTF-8">
 <link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Raleway:300,400,600">

 <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>


 <script> 
    $(function(){
      $("#includedContent").load("header.php");
      $("#footer").load("footer.php"); 
    });
</script>

 <title>KU Admisstion Test-2017</title>
</head>

  <div id="includedContent">
   </div>

<body>

  <div class="container">

    <div class="col-sm-offset-2 col-sm-8">

      <div class="panel panel-primary">

        <div class="panel-heading"> 
          Fill the informations        
        </div>

        <div class="panel-body">
        
        <?php

include 'connection.php';

$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
// if($imageFileType != "jpg"  && $imageFileType != "png"  && $imageFileType != "jpeg" && $imageFileType != "gif" )  {
//     echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
//     $uploadOk = 0;
// }

$file_type = $_FILES['fileToUpload']['type']; //returns the mimetype

$allowed = array("image/jpeg", "image/gif", "image/jpg", "image/JPG");
if(!in_array($file_type, $allowed)) {
  $error_message = 'Only jpg/JPG, gif and jpeg files are allowed.';
  $error = 'yes';
  echo $error_message;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    $filename = $_FILES["fileToUpload"]["tmp_name"];
    //$extension = end(explode(".", $filename));
    $newfilename = "".$_SESSION['app_no'].".".$imageFileType;
    print_r($newfilename);
    if (move_uploaded_file($filename, "uploads/" .$newfilename)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    $var = "uploads/".$newfilename;
        echo '<br>';
        echo '<img src="'.$var.'"  height="180" width="180"> '; 
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>
  

          
      

    </div>
  </div>


</div>  

</div>
<div id="footer">
  
</div>

</body>

<script src="js/bootstrap.min.js"></script>
</html>