<?php

$servername = "localhost"; // set your server address.
$username = "root"; // set your database username 
$password = ""; // give your db user password 


$dbname = "admission_test"; // change this to your data base name



if (session_status() == PHP_SESSION_NONE) 
	{
		ob_start();
		session_start();

	}

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>