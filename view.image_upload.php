<!DOCTYPE html>
<html>
<head>
    <title>Khulna University</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <meta charset="UTF-8">
    <link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Raleway:300,400,600">

    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script> 
        $(function(){
            $("#includedContent").load("header.php");
            $("#footer").load("footer.php"); 
        });
    </script>
</head>

<div id="includedContent">
</div>
<body>

    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    Application ID
                </div>
                <div class="panel-body">
                    
                <div id="includedContent">
                    </div>
                        <form action="upload.php" method="post" enctype="multipart/form-data">
                        Select image to upload:
                        <input type="file" name="fileToUpload" id="fileToUpload">
                        <input type="submit" value="Upload Image" name="submit">
                    </form>


                </div>
            </div>
            
        </div>
        
    </div>


    <div id="footer">
        
    </div>

</body>
</html>



