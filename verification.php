<!DOCTYPE html>
<html>
<head>
	<title>Khulna University</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Raleway:300,400,600">

	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script> 
		$(function(){
			$("#includedContent").load("header.php");
			$("#footer").load("footer.php"); 
		});
	</script>
</head>

<div id="includedContent">
</div>
<body>

	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">

			<div class="panel panel-primary">
				<div class="panel-heading">
					Credentials For AdmitCard
				</div>
				<div class="panel-body">
					
				<?php
				include 'Util.php';
				include 'connection.php';

				$util = new Util;

				$username = $util->random_str(6,'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
				$password = $util->random_str(4);

				echo $util->check_user_pass_exists_or_create(mysqli_real_escape_string($conn,$_POST['app_no']),$username, $password);

				//insert application no, username, password in a table 
				//and change payment status

				
				echo '<a href="view.login.php"> <h3> Login </h3> </a>';
			    ?>

				</div>
			</div>
			
		</div>
		
	</div>


	<div id="footer">
		
	</div>

</body>
</html>







