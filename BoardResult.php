<?php

/**
* 
*/
class BoardResult
{
    private $_name; 
    private $_fname;
    private $_mname;

    
    private $_ssc_reg;
    private $_ssc_session;
    private $_ssc_roll;
    private $_ssc_pass_year;
    private $_ssc_board_code;
    private $_ssc_board_name;
    private $_ssc_dob_name;
    private $_ssc_i_code;
    private $_ssc_eiin;
    private $_ssc_gpa;
    private $_ssc_gpa_exc4th;
    private $_ssc_group;
    private $_ssc_ltrgrd;
    private $_ssc_ltrgrd_4s;
    private $_ssc_c_type;
    private $_ssc_result;
    
    
    

    
    private $_hsc_reg;
    private $_hsc_session; 
    private $_hsc_roll;
    private $_hsc_pass_year;
    
    private $_hsc_board_code; 
    private $_hsc_board_name;
    private $_hsc_i_code;
    private $_hsc_eiin; 
    private $_hsc_gpa;
    private $_hsc_ltrgrd;
    private $_hsc_gpa_exc4th; 
    private $_hsc_group; 
    private $_sex;
    
    
    
    
    private $_hsc_ltrgrd_4s;
    private $_hsc_result;
    
    
    private $_hsc_c_type; 
    
    

    

    function BoardResult()
    {

    }

    public function set_name ( $name) {
        $this->_name = $name;
    }

    public function get_name () {
        return $this->_name;
    }

    public function set_fname ( $fname) {
        $this->_fname = $fname;
    }

    public function get_fname () {
        return $this->_fname;
    }
    
    public function set_mname ( $mname) {
        $this->_mname = $mname;
    }

    public function get_mname () {
        return $this->_mname;
    }
    

    public function set_ssc_reg ( $ssc_reg ) {
        $this->_ssc_reg = $ssc_reg;
    }

    public function get_ssc_reg () {
        return $this->_ssc_reg;
    }


    public function get_ssc_session () {
        return $this->_ssc_session;
    }


    public function set_ssc_session ( $ssc_session ) {
        $this->_ssc_session = $ssc_session;
    }

    public function set_ssc_roll ( $ssc_roll ) {
        $this->_ssc_roll = $ssc_roll;
    }

    public function get_ssc_roll () {
        return $this->_ssc_roll;
    }

    public function set_ssc_pass_year ( $ssc_pass_year ) {
        $this->_ssc_pass_year = $ssc_pass_year;
    }

    public function get_ssc_pass_year () {
        return $this->_ssc_pass_year;
    }




    public function get_ssc_board_code () {
        return $this->_ssc_board_code;
    }


    public function set_ssc_board_code ( $ssc_board_code) {
        $this->_ssc_board_code = $ssc_board_code;
    }
    public function get_ssc_board_name () {
        return $this->_ssc_board_name;
    }


    public function set_ssc_board_name ( $ssc_board_name) {
        $this->_ssc_board_name = $ssc_board_name;
    }


    public function set_ssc_dob_name ( $ssc_dob_name) {
        $this->_ssc_dob_name = $ssc_dob_name;
    }
    public function get_ssc_dob_name () {
        return $this->_ssc_dobb_name;
    }

    public function get_ssc_i_code () {
        return $this->_ssc_i_code;
    }
    public function set_ssc_i_code( $ssc_i_code) {
        $this->_ssc_i_code = $ssc_i_code;
    }

    public function get_ssc_eiin () {
        return $this->_ssc_eiin;
    }
    public function set_ssc_eiin ( $eiin ) {
        $this->_ssc_eiin = $eiin;
    }

    public function get_ssc_gpa () {
        return $this->_ssc_gpa;
    }


    public function set_ssc_gpa ( $ssc_gpa) {
        $this->_ssc_gpa = $ssc_gpa;
    }

    public function get_ssc_gpa_exc4th () {
        return $this->_ssc_gpa_exc4th;
    }


    public function set_ssc_gpa_exc4th ( $ssc_gpa_exc4th) {
        $this->_ssc_gpa_exc4th = $ssc_gpa_exc4th;
    }


    public function get_ssc_group () {
        return $this->_ssc_group;
    }


    public function set_ssc_group( $ssc_group) {
        $this->_ssc_group = $ssc_group;
    }

    public function set_ssc_ltrgrd ( $ssc_ltrgrd ) {
        $this->_ssc_ltrgrd = $ssc_ltrgrd;
    }

    public function get_ssc_ltrgrd () {
        return $this->_ssc_ltrgrd;
    }
    public function get_ssc_ltrgrd_4s () {
        return $this->_ssc_ltrgrd_4s;
    }
    public function set_ssc_ltrgrd_4s ( $ssc_ltrgrd_4s ) {
        $this->_ssc_ltrgrd_4s = $ssc_ltrgrd_4s;
    }

    public function get_ssc_c_type () {
        return $this->_ssc_c_type;
    }
    public function set_ssc_c_type ( $c_type ) {
        $this->_ssc_c_type = $c_type;
    }


    public function get_ssc_result () {
        return $this->_ssc_result;
    }

    public function set_ssc_result ( $ssc_result ) {
        $this->_ssc_result = $ssc_result;
    }


    
    
//HSC
    public function set_hsc_reg ( $hsc_reg ) {
        $this->_hsc_reg = $hsc_reg;
    }

    public function get_hsc_reg () {
        return $this->_hsc_reg;
    }


    public function get_hsc_session () {
        return $this->_hsc_session;
    }

    public function set_hsc_session ( $hsc_session ) {
        $this->_hsc_session = $hsc_session;
    }



    public function get_hsc_roll () {
        return $this->_hsc_roll;
    }

    public function set_hsc_roll ( $hsc_roll ) {
        $this->_hsc_roll = $hsc_roll;
    }



    public function set_hsc_pass_year ( $hsc_pass_year ) {
        $this->_hsc_pass_year = $hsc_pass_year;
    }

    public function get_hsc_pass_year () {
        return $this->_hsc_pass_year;
    }


    public function get_hsc_board_code () {
        return $this->_hsc_board_code;
    }


    public function set_hsc_board_code ( $hsc_board_code) {
        $this->_hsc_board_code = $hsc_board_code;
    }

    public function get_hsc_board_name () {
        return $this->_hsc_board_name;
    }


    public function set_hsc_board_name ( $hsc_board_name) {
        $this->_hsc_board_name = $hsc_board_name;
    }



    public function get_hsc_i_code () {
        return $this->_hsc_i_code;
    }
    public function set_hsc_i_code( $hsc_i_code) {
        $this->_hsc_i_code = $hsc_i_code;
    }


    public function get_eiin () {
        return $this->_hsc_eiin;
    }
    public function set_eiin ( $eiin ) {
        $this->_hsc_eiin = $eiin;
    }
    
    public function get_hsc_gpa () {
        return $this->_hsc_gpa;
    }


    public function set_hsc_gpa ( $hsc_gpa) {
        $this->_hsc_gpa = $hsc_gpa;
    }



    public function set_hsc_ltrgrd ( $hsc_ltrgrd ) {
        $this->_hsc_ltrgrd = $hsc_ltrgrd;
    }

    public function get_hsc_ltrgrd () {
        return $this->_hsc_ltrgrd;
    }

    public function get_hsc_ltrgrd_4s () {
        return $this->_hsc_ltrgrd_4s;
    }
    public function set_hsc_ltrgrd_4s ( $hsc_ltrgrd_4s ) {
        $this->_hsc_ltrgrd_4s = $hsc_ltrgrd_4s;
    }

    public function get_hsc_group () {
        return $this->_hsc_group;
    }


    public function set_hsc_group( $hsc_group) {
        $this->_hsc_group = $hsc_group;
    }

    public function get_hsc_gpa_exc4th () {
        return $this->_hsc_gpa_exc4th;
    }


    public function set_hsc_gpa_exc4th ( $hsc_gpa_exc4th) {
        $this->_hsc_gpa_exc4th = $hsc_gpa_exc4th;
    }


    public function get_sex () {
        return $this->_sex;
    }
    public function set_sex ($sex) {
        $this->_sex = $sex;
    }


    public function get_hsc_c_type () {
        return $this->_c_type;
    }
    public function set_hsc_c_type ( $c_type ) {
        $this->_c_type = $c_type;
    }



    public function get_hsc_result () {
        return $this->_hsc_result;
    }

    public function set_hsc_result ( $hsc_result ) {
        $this->_hsc_result = $hsc_result;
    }

}


?>