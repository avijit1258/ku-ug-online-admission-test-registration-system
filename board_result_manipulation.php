<?php

include 'BoardResult.php';
include 'connection.php';



$hsc_roll = mysqli_real_escape_string($conn,$_POST['hsc_roll_no']);
$hsc_reg = mysqli_real_escape_string($conn,$_POST['hsc_reg_no']);
$hsc_pass_year = mysqli_real_escape_string($conn,$_POST['hsc_pass_year']);
$hsc_board = mysqli_real_escape_string($conn,$_POST['hsc_board']);

$ssc_roll = mysqli_real_escape_string($conn,$_POST['ssc_roll_no']);
$ssc_reg = mysqli_real_escape_string($conn,$_POST['ssc_reg_no']);
$ssc_pass_year = mysqli_real_escape_string($conn,$_POST['ssc_pass_year']);
$ssc_board = mysqli_real_escape_string($conn,$_POST['ssc_board']);

$h = $hsc_board.'h'.$hsc_pass_year;//compostie of rajh20116
$s = $ssc_board.'s'.$ssc_pass_year;//composite of rajs2014
//rajh2016
$sql_hsc = "SELECT regno, roll_no, session, pass_year, board_name,gpa,gpa_exc4th, ltrgrd, ltrgrd_4s,hsc_group,ssc_regno, ssc_roll FROM $h WHERE regno = '$hsc_reg' AND roll_no = '$hsc_roll'";

//rajs2014
$sql_ssc = "SELECT name,fname, mname, regno,session,roll_no,pass_year, board_name,dob,gpa,gpa_exc4th,ssc_group, ltrgrd, ltrgrd_4s FROM $s WHERE regno = '$ssc_reg' AND roll_no = '$ssc_roll'";


try {
	$result_hsc = mysqli_query($conn, $sql_hsc);
	$result_ssc = mysqli_query($conn, $sql_ssc);
	$row_hsc = mysqli_fetch_assoc($result_hsc);
	$row_ssc = mysqli_fetch_assoc($result_ssc);
}
catch (MySQLDuplicateKeyException $e) {
    // duplicate entry exception
    var_dump($e->getMessage());
}
catch (MySQLException $e) {
    // other mysql exception (not duplicate key entry)
    var_dump($e->getMessage());
}
catch (Exception $e) {
    // not a MySQL exception
    var_dump($e->getMessage());
}

$BoardResult = new BoardResult();


$BoardResult->set_name($row_ssc['name']); 
$BoardResult->set_fname($row_ssc['fname']); 
$BoardResult->set_mname($row_ssc['mname']); 
$BoardResult->set_ssc_roll($row_ssc['roll_no']); 
$BoardResult->set_ssc_reg($row_ssc['regno']);
$BoardResult->set_ssc_session($row_ssc['session']); 
// $BoardResult->set_ssc_board_code($row_ssc['board_code']);
$BoardResult->set_ssc_board_name($row_ssc['board_name']);
$BoardResult->set_ssc_dob_name($row_ssc['dob']); 
// $BoardResult->set_ssc_i_code($row_ssc['i_code']); 
// $BoardResult->set_ssc_session($row_ssc['eiin']); 
// $BoardResult->set_ssc_roll($row_ssc['roll_no']);
$BoardResult->set_ssc_gpa($row_ssc['gpa']);
$BoardResult->set_ssc_gpa_exc4th($row_ssc['gpa_exc4th']);
$BoardResult->set_ssc_group($row_ssc['ssc_group']);
$BoardResult->set_ssc_ltrgrd($row_ssc['ltrgrd']); 
$BoardResult->set_ssc_ltrgrd_4s($row_ssc['ltrgrd_4s']); 
$BoardResult->set_ssc_pass_year($row_ssc['pass_year']); 
// $BoardResult->set_ssc_c_type($row_ssc['c_type']);
// $BoardResult->set_ssc_result($row_ssc['result']);

//redundante
// $BoardResult->set_name($row_hsc['name']); 
// $BoardResult->set_fname($row_hsc['fname']); 
// $BoardResult->set_mname($row_hsc['mname']); 
$BoardResult->set_hsc_reg ($row_hsc['regno']);
$BoardResult->set_hsc_session($row_hsc['session']); 
$BoardResult->set_hsc_roll($row_hsc['roll_no']);
$BoardResult->set_hsc_pass_year($row_hsc['pass_year']);
// $BoardResult->set_hsc_board_code($row_hsc['board_code']);
$BoardResult->set_hsc_board_name($row_hsc['board_name']); 
// $BoardResult->set_hsc_i_code($row_hsc['i_code']); 
// $BoardResult->set_eiin($row_hsc['eiin']);
$BoardResult->set_hsc_gpa($row_hsc['gpa']); 
$BoardResult->set_hsc_gpa_exc4th($row_hsc['gpa_exc4th']); 
$BoardResult->set_hsc_ltrgrd($row_hsc['ltrgrd']); 
$BoardResult->set_hsc_ltrgrd_4s($row_hsc['ltrgrd_4s']); 
$BoardResult->set_hsc_group($row_hsc['hsc_group']);
// $BoardResult->set_sex($row_hsc['sex']); 
// $BoardResult->set_hsc_c_type($row_hsc['c_type']); 
// $BoardResult->set_hsc_result($row_hsc['result']);


$_SESSION['result_object'] = $BoardResult;

?>