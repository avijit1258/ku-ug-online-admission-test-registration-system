-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2017 at 07:49 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admission_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `rajh2016`
--

CREATE TABLE `rajh2016` (
  `name` varchar(40) DEFAULT NULL,
  `fname` varchar(40) DEFAULT NULL,
  `mname` varchar(40) DEFAULT NULL,
  `regno` varchar(10) DEFAULT NULL,
  `session` varchar(9) DEFAULT NULL,
  `roll_no` varchar(6) DEFAULT NULL,
  `pass_year` varchar(4) DEFAULT NULL,
  `board_code` varchar(2) DEFAULT NULL,
  `board_name` varchar(20) DEFAULT NULL,
  `i_code` varchar(5) DEFAULT NULL,
  `eiin` varchar(6) DEFAULT NULL,
  `gpa` varchar(4) DEFAULT NULL,
  `gpa_exc4th` varchar(4) DEFAULT NULL,
  `ltrgrd` varchar(70) DEFAULT NULL,
  `ltrgrd_4s` varchar(25) DEFAULT NULL,
  `hsc_group` varchar(20) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `ssc_regno` varchar(10) DEFAULT NULL,
  `ssc_session` varchar(9) DEFAULT NULL,
  `ssc_roll` varchar(6) DEFAULT NULL,
  `ssc_passyr` varchar(4) DEFAULT NULL,
  `ssc_board` varchar(10) DEFAULT NULL,
  `c_type` varchar(15) DEFAULT NULL,
  `result` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;