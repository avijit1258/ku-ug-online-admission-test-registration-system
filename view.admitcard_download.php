

<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Kalam' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Cinzel Decorative' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Alegreya Sans SC' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Aldrich' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat Subrayada' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Farsan' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Gudea' rel='stylesheet'>
</head>

<style>
    body {
        height: 842px;
        width: 595px;
        /* to make A4 size page...just for u avi */
        margin-left: auto;
        margin-right: auto;
    }
</style>

<body>

<?php
include_once 'Util.php';
include_once 'connection.php';

$util = new Util;
$application_info = $util->get_application_info();

?>
<form name="StudentRegistration">

    <div style="position:relative; font-family: 'Trebuchet MS', sans-serif; ">

        <img src="logo.png" width="auto" height="100px" style="position:absolute; margin-left:auto;margin-right: auto;">

        <center style="position:absolute; margin-left:130px; margin-right: auto; font-family:'Cinzel Decorative'; ">
            <font size=5 ><b>Khulna University</b></font>
            <br>
            <font size=3>Admission Test (2016-2017)</font>
        </center>
        <br><br>
        <center style="position:absolute; margin-left:110px; margin-top: 30px; ">
            <font size=3><b>Science, Engineering & Technology School</b></font>
        </center>
    </div>

    <?php
    echo '
    <div style="position:relative; margin-top:70px;">

        <img src="uploads/'.$_SESSION['app_no'].'.jpg" width="200px" height="240px" style="position:absolute; margin-left:195px;margin-right: auto; margin-top:35px; opacity:0.2;box-shadow: 4px 4px 5px #aaa; ">

        <img src="uploads/'.$_SESSION['app_no'].'.jpg" width="auto" height="150px" style="position:absolute; margin-left:470px;margin-top: 32px; border: .5px solid dimgray ;border-radius: 5px;">

        <img src="logo.png" width="auto" height="120px" style="position:absolute; margin-left:420px;margin-right: auto; margin-top:120px; opacity:0.3;">
        ';
    ?>
        <hr/>

        <table cellpadding="2" align="center" cellspacing="2" frame="none" style="position: absolute;font-family:'Alegreya Sans SC'; font-size:16px;">

            <tr>
                <center>
                    <font style="font-family: 'Montserrat Subrayada'; font-size:22px; color:DimGray ;">
                        Admit Card
                    </font>
                </center>
            </tr>

            <tr>
                <td>Roll No</td>
                <td>:</td>
                <td style="font-family:Gudea;"><b>1101 <!-- Here goes roll no --></b></td>
            </tr>

            <tr>
                <td>Application ID</td>
                <td>:</td>
                <td style="font-family:Gudea;"><b> <?php echo $_SESSION['app_no']; ?> <!-- Here goes Application ID--></b></td>
            </tr>

            <tr>
                <td>Name</td>
                <td>:</td>
                <td style="font-size:19px;"> <?php echo $application_info['NAME']; ?> <!-- Here goes Name --></td>
            </tr>

            <tr>
                <td>Father Name</td>
                <td>:</td>
                <td style="font-size:19px;"><?php echo $application_info['SFNAME']; ?><!-- Here goes Father Name --></td>
            </tr>

            <tr>
                <td>Mother Name</td>
                <td>:</td>
                <td style="font-size:19px;"><?php echo $application_info['SMNAME']; ?><!-- Here goes Mother Name --></td>
            </tr>

            <tr>
                <td>Exam Date</td>
                <td>:</td>
                <td style="font-family:Gudea; font-size:14px;">03-11-2016 ; 9.00 AM - 11 AM<!-- Here goes Exam Date --></td>
            </tr>
        </table>

        <table cellpadding="2" align="center" cellspacing="2" style="position: absolute;margin-top:170px; font-family:'Alegreya Sans SC', sans-serif; font-size:16px;">
            <tr>
                <td>Exam Centre&emsp;</td>
                <td>:</td>
                <td>Room #3219 , Academic Building #3, Khulna University<!--Here goes the Exam Center Name --></td>
            </tr>
        </table>

        <br><br>

        <table cellpadding="2" align="center" cellspacing="6" style="position: absolute;margin-top:180px; font-family: 'Kalam', sans-serif;">
            <tr>
                <td>
                    ____________________&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;____________________
                </td>
            </tr>

            <tr>
                <td>
                    &emsp;Applicant's Signature&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Invigilator's Signature
                </td>
            </tr>
        </table>

        <table cellpadding="2" align="center" cellspacing="6" style="border: 2px solid dimgray;
		    border-radius: 10px; position: absolute;margin-top:260px; font-family:Gudea; font-size:15px;">
            <tr>
                <td>
                    <b>
                        Important Notice:
                    </b>
                </td>
            </tr>

            <tr>
                <td>
                    01.The candidate must use black ink ball point pen.
                </td>
            </tr>

            <tr>
                <td>
                    02. The candidate is not allowed to carry books, bags, mobile phones, or any other communication devices inside the exam hall.
                </td>
            </tr>
        </table>

        <p style="position: absolute;margin-top:550px;">
            <b>
                <u style="font-family: 'Aldrich';font-size: 14px;">
                    <button onclick="myFunction()">
                        Please print this admit card in colour on an A4 paper and bring 2 (two) copies of it.
                    </button></body>

                </u>
            </b>
        </p>
    </div>
</form>
<script>
    function myFunction() {
        window.print();
    }
</script>
</body>
</html>
