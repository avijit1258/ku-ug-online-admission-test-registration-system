<?php
include 'connection.php';
include 'board_result_manipulation.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<title>KU Admission Test-2017</title>
	<script> 
		$(function(){
			$("#includedContent").load("header.php"); 
		});
	</script>
</head>
<body>

	<div id="includedContent">
	</div>


	
	<?php


	if(($row_ssc["regno"] == $row_hsc["ssc_regno"]) && ($row_ssc["roll_no"] == $row_hsc["ssc_roll"])){
		echo 

		'
		<div class="container">
			<div class="col-sm-offset-2 col-sm-8">
			<div class="panel panel-primary">

        <div class="panel-heading"> 
          Registration Form : step 2         
        </div>

				<form action="confirm.php" method="post"> 
					
					<div class="panel-body">

						<div class="form-group row">
							<label class = "col-sm-3 control-label">Name</label>
							<div class="col-sm-6"> 
								<input class="form-control" type="text" placeholder="Name" value="'.$_SESSION['result_object']->get_name().'"  readonly>

							</div> 
							
						</div>
						<div class="form-group row">
							<label class = "col-sm-3 control-label">Mother</label>
							<div class="col-sm-6"> 
								<input class="form-control" type="text" placeholder="Mothers Name" value = "'.$_SESSION['result_object']->get_mname().'"readonly>

							</div> 
							
						</div>
						<div class="form-group row">
							<label class = "col-sm-3 control-label">Name</label>
							<div class="col-sm-6"> 
								<input class="form-control" type="text" placeholder="Name" value="'.$_SESSION['result_object']->get_fname().'" readonly>

							</div> 
							
						</div>
						<table class="table table-hover table-striped table-bordered">
							<thead>
								<tr><th>EXAM</th><th>Board</th><th>Letter Grade </th><th>GPA</th></tr>
							</thead>
							<tbody>
								';

								echo "<tr>
								<td> HSC  </td>
								<td> ". $row_hsc["board_name"]. "</td> <td> " . $row_hsc["ltrgrd"] ."</td> <td>
								" .$row_hsc["gpa"]."</td>

							</tr>";

							echo "<tr>
							<td>SSC</td>
							<td>". $row_ssc["board_name"]. "</td>
							<td> " . $row_ssc["ltrgrd"] ."</td> <td>
							" .$row_ssc["gpa"]."</td>

						</tr>";
						echo '
					</tbody>
				</table>';			

				echo '
				<div class="form-group row">
					<label class="col-sm-3 control-label">Mobile</label>
					<div class="col-sm-6"> 
						<input type="text" required="true" placeholder="Mobile No" class="form-control" name="mobile_no">

					</div> 
				</div>

				<div class="form-group row">
					<label class="col-sm-3 control-label"> Home District</label>
					<div class="col-sm-6"> 

						<select name="home_district" class="form-control">
							<option value="Bagerhat">Bagerhat</option>
							<option value="Bagerhat">Bagerhat</option>
							<option value="Bandarban">Bandarban</option>
							<option value="Barisal">Barisal</option>
							<option value="Bhola">Bhola</option>
							<option value="Bogra">Bogra</option>
							<option value="Brahmanbaria">Brahmanbaria</option>
							<option value="Chandpur">Chandpur</option>
							<option value="Chapainawabganj">Chapainawabganj</option>
							<option value="Chittagong">Chittagong</option>
							<option value="Chuadanga">Chuadanga</option>
							<option value="Comilla"> Comilla</option>
							<option value="Coxs Bazar">Coxs Bazar </option>
							<option value="Dhaka">Dhaka</option>
							<option value="Dinajpur">Dinajpur</option>
							<option value="Faridpur">Faridpur</option>
							<option value="Feni">Feni</option>
							<option value="Gaibandha">Gaibandha</option>
							<option value="Gazipur">Gazipur</option>
							<option value="Habiganj">Habiganj</option>
							<option value="Jessore">Jessore</option>
							<option value="Jhalokati">Jhalokati</option>
							<option value="Jhenaidah">Jhenaidah</option>
							<option value="Joypurhat">Joypurhat</option>
							<option value="Khagrachhari">Khagrachhar</option>
							<option value="Khulna">Khulna</option>
							<option value="Kishoreganj">Kishoreganj</option>
							<option value="Kurigram">Kurigram</option>
							<option value="Kushtia">Kushtia</option>
							<option value="Lakshmipu">Lakshmipur</option>
							<option value="Lalmonirhat">Lalmonirhat</option>
							<option value="Madaripur">Madaripur</option>
							<option value="Magura">Magura</option>
							<option value="Manikganj">Manikganj</option>
							<option value="Meherpur">Meherpur </option>
							<option value="Moulvibazar">Moulvibazar</option>
							<option value="Munshiganj">Munshiganj</option>
							<option value="Mymensingh">Mymensingh</option>
							<option value="Naogaon">Naogaon</option>
							<option value="Narail">Narail</option>
							<option value="Narayanganj">Narayanganj</option>
							<option value="Narsingdi"> Narsingdi</option>
							<option value="Natore">Natore</option>
							<option value="Netrakona">Netrakona</option>
							<option value="Nilphamari">Nilphamari</option>
							<option value="Noakhali">Noakhali</option>
							<option value="Pabna">Pabna</option>
							<option value="Panchagarh">Panchagarh</option>
							<option value="Patuakhali">Patuakhali</option>
							<option value="Pirojpur">Pirojpur</option>
							<option value="Rajbari">Rajbari</option>
							<option value="Rajshahi">Rajshahi</option>
							<option value="Rangamati">Rangamati</option>
							<option value="Rangpur">Rangpur</option>
							<option value="Satkhira">Satkhira</option>
							<option value="Shariatpur">Shariatpur</option>
							<option value="Sherpur">Sherpur</option>
							<option value="Sirajganj">Sirajganj</option>
							<option value="Sunamganj">Sunamganj</option>
							<option value="Sylhet">Sylhet</option>
							<option value="Tangail">Tangail</option>
							<option value="Thakurgaon">Thakurgaon</option>
							
						</select>
					</div> 


				</div>

				<div class="form-group  row">
					<label class="col-sm-3 control-label">Email</label>
					<div class="col-sm-6"> 
						<input type="email" placeholder="Email" class="form-control" required="true" name="email">
					</div> 
				</div>

				<div class="form-group  row">
					<label class="col-sm-3 control-label">Quota</label>
					<div class="col-sm-6"> 
						<select name="quota" class="form-control">
							<option value="ff"> Freedom Fighter </option>
							<option value="tr"> Tribunal
							</option>
							<option value=""selected>None
							</option>
						</select>
					</div> 
				</div>

				<fieldset class="form-group">
					<legend>Select School</legend>
					<div class="form-check">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="school" id="set" value="SET" >
							SET School
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="school" id="life" value="LIFE">
							Life Science School
						</label>
					</div>
					<div class="form-check">
						<label class="form-check-label">
							<input type="radio" class="form-check-input" name="school" id="social" value="SOCIAL" >
							Social Science
						</label>
					</div>
				</fieldset>
				<div class="alert alert-danger form-group row" class="col-sm-3" >
					<div id="criteria_choice" >

					</div>
				</div>

			</form>	
		</div>
	</div>
	</div>
</div>

';


mysqli_close($conn);

echo '
';
}
else{

	echo '<h1><a href="index.php">Sorry Try Again</a></h1>';
}

?>



<script src="js/bootstrap.min.js"></script>
<script src="js/requirement_check.js"></script>

</body>


</html>


