<?php
	include_once 'BoardResult.php';
	include 'connection.php';
?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<title>Khulna University</title>
	<h1>
		<center>Filled Registration Form </center>
	</h1>
</head>
<body>

	<?php

	
	
	echo 

	'
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">
			<form action="RegisterHandler.php" method="post"> 

				<div class="panel-body">

					<div class="form-group row">
						<label class = "col-sm-3 control-label">Name</label>
						<div class="col-sm-6"> 
							<input class="form-control" type="text" placeholder="Name" value="'.$_SESSION['result_object']->get_name().'"  readonly>

						</div> 

					</div>
					<div class="form-group row">
						<label class = "col-sm-3 control-label">Mother</label>
						<div class="col-sm-6"> 
							<input class="form-control" type="text" placeholder="Mothers Name" value = "'.$_SESSION['result_object']->get_mname().'"readonly>

						</div> 

					</div>
					<div class="form-group row">
						<label class = "col-sm-3 control-label">Name</label>
						<div class="col-sm-6"> 
							<input class="form-control" type="text" placeholder="Name" value="'.$_SESSION['result_object']->get_fname().'" readonly>

						</div> 

					</div>
					<table class="table table-hover table-striped table-bordered">
						<thead>
							<tr><th>EXAM</th><th>Board</th><th>Letter Grade </th><th>GPA</th></tr>
						</thead>
						<tbody>
							<tr>
								<td> HSC  </td>
								<td> '.$_SESSION['result_object']->get_hsc_board_name().'</td> <td> '.$_SESSION['result_object']->get_hsc_ltrgrd().'</td> <td>
								'.$_SESSION['result_object']->get_hsc_gpa().'</td>

							</tr>
							<tr>
								<td>SSC</td>
								<td>'.$_SESSION['result_object']->get_ssc_board_name().'</td>
								<td> '.$_SESSION['result_object']->get_ssc_ltrgrd().'</td>
								<td>'.$_SESSION['result_object']->get_ssc_gpa().'</td>

							</tr>

						</tbody>
					</table>			

					<div class="form-group row">
						<label class="col-sm-3 control-label">Mobile</label>
						<div class="col-sm-6"> 
							<input type="text" placeholder="Mobile No" class="form-control" name="mobile_no" value="'.$_POST['mobile_no'].'" readonly>

						</div> 
					</div>

					<div class="form-group row">
						<label class="col-sm-3 control-label"> Home District</label>
						<div class="col-sm-6"> 
							<input type="text" placeholder="home district"
							class="form-control"
							name="home_district" value="'.$_POST['home_district'].'"readonly>
						</div> 
					</div>

					<div class="form-group  row">
						<label class="col-sm-3 control-label">Email</label>
						<div class="col-sm-6"> 
							<input type="email" placeholder="Email" class="form-control" name="email"
							value="'.$_POST['email'].'"readonly>
						</div> 
					</div>

					<div class="form-group  row">
						<label class="col-sm-3 control-label">Quota</label>
						<div class="col-sm-6"> 
							<input type="text"  class="form-control" name="quota"
							value="'.$_POST['quota'].'"readonly>
						</div>

					</div>
					<div class="form-group  row">
						<label class="col-sm-3 control-label">School</label>
						<div class="col-sm-6"> 
							<input type="text"  class="form-control" name="school"
							value="'.$_POST['school'].'"readonly>
						</div>

					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-6">
						<button type="submit" class="btn btn-success">
								<i class="fa fa-btn fa-plus"></i>Confirm
							</button>
						</div>
					</div>


				</form>	
			</div>
		</div>

	</div>

	';


	?>


	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>


</body>


</html>
