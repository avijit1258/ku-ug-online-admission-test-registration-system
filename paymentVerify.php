<!DOCTYPE html>
<html>
<head>
	<title>Khulna University</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<meta charset="UTF-8">
	<link rel="stylesheet"  href="https://fonts.googleapis.com/css?family=Raleway:300,400,600">
	<link rel="stylesheet" href="css/styles.css">


	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script> 
		$(function(){
			$("#includedContent").load("header.php");
			$("#footer").load("footer.php"); 
		});
	</script>
</head>

<div id="includedContent">
</div>
<body>
	<h1> </h1>
	<div class="container">
		<div class="col-sm-offset-2 col-sm-8">

			<div class="panel panel-primary">
				<div class="panel-heading">
					Verify Your Payment
				</div>
				<div class="panel-body">
					<form action="verification.php" method="post" class="form-horizontal">
						<div class="form-group">
							<label class="col-sm-3 control-label" >Application NO</label>
							<div class="col-sm-6">
								<input type="text" name="app_no" value="" class="form-control"placeholder="Application No">

							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" >Date Of Birth</label>
							<div class="col-sm-6">
								<input type="date" name="dob" value="" class="form-control">

							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-3 control-label" >Transaction ID</label>
							<div class="col-sm-6">
								<input type="text" name="transaction_id" value="" class="form-control"placeholder="Transaction ID">

							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-5 col-sm-6">
								<button type="submit" class="btn btn-success">
									<i class="fa fa-btn fa-plus"></i>Submit
								</button>
							</div>
						</div>
					</form>


				</div>
			</div>

		</div>

	</div>


	<div id="footer">

	</div>

</body>
</html>