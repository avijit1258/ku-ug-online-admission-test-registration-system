CREATE TABLE IF NOT EXISTS rajs2014 
( 
	name VARCHAR(40), 
	fname VARCHAR(40),
	mname VARCHAR(40),
	regno VARCHAR(10),
	session VARCHAR(9),
	roll_no VARCHAR(6),
	pass_year VARCHAR(4),
	board_code VARCHAR(9),
	board_name VARCHAR(20),
	dob VARCHAR(8),
	i_code VARCHAR(5),
	eiin VARCHAR(6),
	gpa VARCHAR(4),
	gpa_exc4th VARCHAR(4),
	ssc_group VARCHAR(20),
	ltrgrd VARCHAR(70),
	ltrgrd_4s VARCHAR(20),
	sex VARCHAR(1),
	c_type VARCHAR(12),
	result VARCHAR(20)
);
