# KU-UG-Online-Admission-Test-Registration-System
We are building Online under-graduate admission test registration system for khulna university

Documentation
-------------
1. At first student have to give their ssc and hsc roll , registration number at **index.html page . So that
we can look for details from our collected database of board results . 

2. After index.html we will go to **view.result_show.php which includes **board_result_manipulation.php . 

In board_result_manipulation.php we have used the input of index.html to get data of a desired student . 
Then for less db hit we put the data in session and for further information of the student we look for the
session object called **BoardResult . 

view.result_show.php
--------------------
It uses the enquired data to check if the given information at index.html page is same student . Next
It shows name , father name , mother name, ssc(board, letregrd , gpa), hsc(board, letregrd , gpa) of the 
concerned student . Then It takes input of phone number , district and email . At last this page has a 
select school feature where three radio button for schools are given . For selecting one student will
see if he is qualified for the school and if yes then a register button containing link of confirmation 
link will come .

BoardResult.php
---------------
This is a class which object is used to store ssc, hsc data to browser session . Its attributes are  
name, father name, mother name and other information of ssc and hsc . 

Requirement_check.js
--------------------
This javascript file just check if select tag "school"s criteria is filled by the student . 
It uses an ajax request for this task . And it sends request to CriteriaHandler.php file . 

CriteriaHandler.php
-------------------

Then it checks the desired school and calls criteria evaluation function for that school on Criteria.php .

Criteria.php
------------
This file contains three function for evaluating validity for each school .

3. If a student is capable of to apply a school then register button will appear to view.result_show.php 
page and after clicking register button form will post request to confirm.php page . 

confirm.php
-----------

This is for finally ensuring given data is ok and lessen database hit with unnecessary users . 
This information is from session variable and post variable .

RegisterHandler.php
-------------------

post request from **confirm.php page to this page . And this page uses Register.php class object
to store data to desired table of database . 

Register.php
------------

This class has a method called set_school which first check if a student already registered if yes then
it provides related information , if not then it stores given data and shows application id and requests 
to verify payment .

4.paymentVerify.php
-------------------

This page takes application id , date of birth and transaction_id to verify payment . 

verification.php
----------------

This page generates username and password for unique application_id .

 



For testing see testData.txt file for demo data . 
