<?php
include_once 'connection.php';
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.html">KU Admission Test-2017</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.html"> <span> Home </span></a></li>
      <li><a href="paymentVerify.php"> <span> Payment Verification</span>
      </a></li>
      <?php

      //var_dump($_SESSION);
      if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true)
      {
        echo '
          <li> <a href="view.image_upload.php" > Image Upload</a> </li>
         <li> <a href="view.admitcard_download.php" > Download Admit Card </a> </li>
         ';
      }            
      ?>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <?php

      if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        //var_dump($_SESSION['username']);
        echo '
        <li>  <h3 style="background-color:green">  '.$_SESSION["username"].' </h3> </li>
        <li><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        ';  
      }
      else{
        echo '
        <li><a href="view.login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> ';
      }
      ?>      
    </ul>
  </div>
</nav>



